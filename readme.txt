Project Name:
		THE QUIZ

Overview of project :
			The project is basically sort of a Quiz or a exam, which provides a user with some questions, and he may increase his score by answering it correctly and get to the top of the score board .... 			

--------------------------------------------------------------------------------
Detailed functionality listing and scope :

To attempt the test , He/She must necessarily login after registration which would be taken care of the authenication speciality of web2py
			
After registration their name would be inserted automatically in to auth_user table. and manually a table which stores his score and no of questions taken by him  under his username is created 

There is a table through which we can manipulate the questions and their answers or insert more questions

All the tables which were not supposed to be used by the user have authenications, and people belonging to a particular admin group can only acess them

There is a scoreboard in the test page which describes the top scorers in the test.. Also we could search any user for their score(Using AJAX)

User can also see his profile on the test page

Also he would must check the countdown time..afterwhich test ceases and his total score would be displayed on big screen

----------------------------------------------------------------------------------
Technologies/frameworks used :

languages  : javascript, Ajax, Python  
Frameworks : web2py 

Javascript is used for option selection and redirection purposes. some of the inbuilt js modules of the web2py are used
Ajax is used for the scoreboard to display the top users, and also for search box
The python functions in the controllers interact with the views to change the questions ,update score board
-----------------------------------------------------------------------------------



